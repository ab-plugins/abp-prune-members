# ABP Prune members
Plugin to automatically prune inctive members

# Installation
Just upload the content of the Upload/ directory to the root of your board, then install and activate.
Think to change the settings !

# Settings
## Plugin:
*  **Group members** : only members having their primary group in one of these groups will be affected
*  **Based search** : you can choose wether you base your search on registration date or on last activity
*  **Days** : will delete users registered before the number of days or with an inactivity greater than the number of days. Minimum is 1 day
*  **Posts** : will only delete users having less than this number of posts
  
## Task:
Just choose how often you want to exceute this task. Default is each first day of month at 03h18 AM