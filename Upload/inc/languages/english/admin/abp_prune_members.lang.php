<?php
$l['abppm_name'] = 'ABP Prune Members';
$l['abppm_desc'] = 'Automatically prune old/inactive members';
$l['abppm_setting_title'] = 'ABP Prune Members settings';
$l['abppm_setting_desc'] = 'Choose prune criterias';

$l['abppm_groups_title'] = 'Members of the following groups...';
$l['abppm_groups_desc'] = 'Select groups in which members may be pruned (<small>primary group</small>)';
$l['abppm_lasttype_title'] = 'Base search on:';
$l['abppm_lasttype_desc'] = 'You can choose to search user upon registered date or last activity date';
$l['typeregdate'] = 'Registration date';
$l['typelastvisit'] = 'Last activity date';

$l['abppm_nbdays_title'] = 'Number of days';
$l['abppm_nbdays_desc'] = 'Since how many days the user has been registered / had his last activity ? (<small>1 is the minimum</small>)';

$l['abppm_post_title'] = 'Number of posts';
$l['abppm_post_desc'] = 'How much posts an user must have to not being pruned ?';

$l['abppm_task_nogroup'] = 'No group selected in settings';
$l['abppm_task_deleted'] = 'ABP Prune Members ended! {1} user(s) deleted';
$l['abppm_task_stopped'] = 'ABP Prune Members stopped : no user to delete';