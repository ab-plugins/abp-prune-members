<?php
/**
 * Automaticaly prune members
 * (c) CrazyCat 2020
 */
define('CN_ABPPM', str_replace('.php', '', basename(__FILE__)));

function task_abp_prune_members($task) {
    global $mybb, $lang, $db;
    $lang->load(CN_ABPPM);
    $err = [];
    $groups = explode(',', $mybb->settings[CN_ABPPM.'_groups']);
    if (count($groups)==0 || (count($groups)==1 && (int)$groups[0]==0)) {
        $err[] = $lang->abppm_task_nogroup;
    }
    $type = $mybb->settings[CN_ABPPM.'_lasttype'];
    $days = $mybb->settings[CN_ABPPM.'_nbdays'];
    $posts = $mybb->settings[CN_ABPPM.'_posts'];
 
    $select = "usergroup IN (".implode(',', $groups).") AND postnum<".(int)$posts." AND ".$type."<".(TIME_NOW - (24*60*60*(int)$days));
    $query = $db->simple_select('users', 'uid, username', $select);
    if ($db->num_rows($query)==0) {
        add_task_log($task, $lang->abppm_task_stopped);
    } else {
        $list=[];
        while ($res = $db->fetch_array($query)) {
            $list[$res['uid']] =  $res['username'];
        }
        require_once MYBB_ROOT.'inc/datahandlers/user.php';
        $userhandler = new UserDataHandler('delete');
        $deleteds = $userhandler->delete_user(array_keys($list), 0);
        add_task_log($task, $lang->sprintf($lang->abppm_task_deleted, $deleteds['deleted_users']));
    }
}