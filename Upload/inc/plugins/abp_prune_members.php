<?php
/**
 * Automaticaly prune members
 * (c) CrazyCat 2020
 */
if (!defined('IN_MYBB')) {
    die('Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.');
}

define('CN_ABPPM', str_replace('.php', '', basename(__FILE__)));

function abp_prune_members_info() {
    global $lang;
    $lang->load(CN_ABPPM);
    return array(
        'name' => $lang->abppm_name,
        'description' => $lang->abppm_desc . '<a href=\'https://ko-fi.com/V7V7E5W8\' target=\'_blank\'><img height=\'30\' style=\'border:0px;height:30px;float:right;\' src=\'https://az743702.vo.msecnd.net/cdn/kofi1.png?v=0\' border=\'0\' alt=\'Buy Me a Coffee at ko-fi.com\' /></a>',
        'website' => 'https://gitlab.com/ab-plugins/abp-prune-members',
        'author' => 'CrazyCat',
        'authorsite' => 'https://community.mybb.com/mods.php?action=profile&uid=6880',
        'version' => '0.2',
        'compatibility' => '18*',
        'codename' => CN_ABPPM
    );
}

function abp_prune_members_install() {
    global $lang, $db;
    $lang->load(CN_ABPPM);
    $settinggroups = ['name' => CN_ABPPM,
        'title' => $lang->abppm_setting_title,
        'description' => $lang->abppm_setting_desc,
        'disporder' => 0,
        "isdefault" => 0
    ];
    $db->insert_query('settinggroups', $settinggroups);
    $gid = $db->insert_id();
    $settings = [
        [
            'name' => CN_ABPPM.'_groups',
            'title' => $lang->abppm_groups_title,
            'description' => $lang->abppm_groups_desc,
            'optionscode' => 'groupselect',
            'value' => '2,5',
            'disporder' => 1
        ],
        [
            'name' => CN_ABPPM.'_lasttype',
            'title' => $lang->abppm_lasttype_title,
            'description' => $lang->abppm_lasttype_desc,
            'optionscode' => 'select
regdate='.$lang->typeregdate.'
lastvisit='.$lang->typelastvisit,
            'value' => '30',
            'disporder' => 2
        ],
        [
            'name' => CN_ABPPM.'_nbdays',
            'title' => $lang->abppm_nbdays_title,
            'description' => $lang->abppm_nbdays_desc,
            'optionscode' => 'numeric
min=1',
            'value' => '30',
            'disporder' => 3
        ],
        [
            'name' => CN_ABPPM.'_posts',
            'title' => $lang->abppm_post_title,
            'description' => $lang->abppm_post_desc,
            'optionscode' => 'numeric',
            'value' => '5',
            'disporder' => 4
        ],
    ];
    foreach ($settings as $setting) {
        $insert = [
            'name' => $db->escape_string($setting['name']),
            'title' => $db->escape_string($setting['title']),
            'description' => $db->escape_string($setting['description']),
            'optionscode' => $db->escape_string($setting['optionscode']),
            'value' => $db->escape_string($setting['value']),
            'disporder' => $setting['disporder'],
            'gid' => $gid,
        ];
        $db->insert_query('settings', $insert);
    }
    rebuild_settings();
}

function abp_prune_members_is_installed() {
    global $mybb;
    return (array_key_exists(CN_ABPPM . '_groups', $mybb->settings));
}

function abp_prune_members_uninstall() {
    global $db;
    $db->delete_query('settings', "name LIKE '" . CN_ABPPM . "_%'");
    $db->delete_query('settinggroups', "name = '" . CN_ABPPM . "'");
    rebuild_settings();
}

function abp_prune_members_activate() {
    global $db, $lang;
    require_once MYBB_ROOT . "inc/functions_task.php";
    $task = [
        'title' => $lang->abppm_name,
        'description' => $lang->abppm_desc,
        'file' => CN_ABPPM,
        'minute' => '18',
        'hour' => '3',
        'day' => '1',
        'month' => '*',
        'weekday' => '*',
        'enabled' => 1,
        'logging' => 1,
        'locked' => 0
    ];
    $task_insert['nextrun'] = fetch_next_run($task);
    $db->insert_query('tasks', $task);
}

function abp_prune_members_deactivate() {
    global $db;
    $db->update_query('tasks', ['enabled'=>0], "file='".CN_ABPPM."'");
}